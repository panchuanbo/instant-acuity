//
//  main.m
//  Instant Acuity
//
//  Created by Perry Alagappan on 2/13/16.
//  Copyright © 2016 TreeHacks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

//
//  AppDelegate.h
//  Instant Acuity
//
//  Created by Perry Alagappan on 2/13/16.
//  Copyright © 2016 TreeHacks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


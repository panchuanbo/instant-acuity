//
//  ViewController.h
//  Instant Acuity
//
//  Created by Perry Alagappan on 2/13/16.
//  Copyright © 2016 TreeHacks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    
}

- (IBAction)dismissKeyboard:(id)sender;


@end


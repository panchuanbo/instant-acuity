//
//  ViewController.m
//  Instant Acuity
//
//  Created by Perry Alagappan on 2/13/16.
//  Copyright © 2016 TreeHacks. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissKeyboard:(id)sender {
    [sender resignFirstResponder];
    
}

@end

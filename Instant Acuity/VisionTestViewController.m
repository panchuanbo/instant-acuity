//
//  VisionTestViewController.m
//  Instant Acuity
//
//  Created by Perry Alagappan on 2/14/16.
//  Copyright © 2016 TreeHacks. All rights reserved.
//

#import "VisionTestViewController.h"

@interface VisionTestViewController ()

@property NSString* speechText;
@property BOOL speechDone;
@property BOOL utteringThings;
@property BOOL allowedToReset;

@end

@implementation VisionTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    orientations = @[@"up", @"down", @"left", @"right"];
    images = [NSMutableArray new];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(watsonSaysHi:) name:@"watson" object:nil];
    
    badDistanceLabel.text = @"Good Distance";
    
    self.speechDone = true;
    self.utteringThings = false;
    self.allowedToReset = true;
    
    startButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    startButton.titleLabel.numberOfLines = 2;
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideShowDebug:)];
    recognizer.direction = UISwipeGestureRecognizerDirectionDown;
    recognizer.numberOfTouchesRequired = 3;
    [self.view addGestureRecognizer:recognizer];
}

- (void)hideShowDebug:(UIGestureRecognizer *)recognizer {
    startButton.hidden = !startButton.hidden;
    finishButton.hidden = !finishButton.hidden;
}

- (void)viewDidAppear:(BOOL)animated {
    [self setupCamera];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    [UIApplication sharedApplication].idleTimerDisabled = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Motion

/* Sets up motion detection to ensure that the camera is relatively stable
 */
- (void)setupMotion {
    motionManager = [[CMMotionManager alloc] init];
    deviceMotion = [[CMDeviceMotion alloc] init];
    
    [motionManager setDeviceMotionUpdateInterval:0.1];
    [motionManager startDeviceMotionUpdates];
    
    [motionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue mainQueue] withHandler:^(CMDeviceMotion *data, NSError *error) {
        CMAttitude *attitude = data.attitude;
        pitch = attitude.pitch;
        yaw = attitude.yaw;
        roll = attitude.roll;
    }];
}

#pragma mark - Facial Detection

/* Sets up camera to detect face to ensure that user isn't moving too close or too far away.
 * User is told that camera is in use.
 */
- (void)setupCamera {
    session = [[AVCaptureSession alloc] init];
    session.sessionPreset = AVCaptureSessionPresetMedium;
    
    NSArray *videoDevices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    if (videoDevices.count != 0) {
        for (AVCaptureDevice *aDevice in videoDevices)
        {
            if (aDevice.position == AVCaptureDevicePositionFront)
            {
                device = aDevice;
                break;
            }
        }
        
        input = [AVCaptureDeviceInput deviceInputWithDevice:device error:nil];
        
        [session addInput:input];
        
        output = [[AVCaptureVideoDataOutput alloc] init];
        
        [session addOutput:output];
        
        dispatch_queue_t queue = dispatch_queue_create("myQueue", NULL);
        [output setSampleBufferDelegate:self queue:queue];
        output.videoSettings = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA] forKey:(id)kCVPixelBufferPixelFormatTypeKey];
        
        [device lockForConfiguration:nil];
        [device setActiveVideoMinFrameDuration:CMTimeMake(1, 15)];
        
        [session startRunning];
        
        NSDictionary *detectorOptions = [[NSDictionary alloc] initWithObjectsAndKeys:CIDetectorAccuracyLow, CIDetectorAccuracy, nil];
        self.faceDetector = [CIDetector detectorOfType:CIDetectorTypeFace context:nil options:detectorOptions];
    }
}


// Delegate routine that is called when a sample buffer was written
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    // Create a UIImage from the sample buffer data
    UIImage *image = [self imageFromSampleBuffer:sampleBuffer];
    image = [[UIImage alloc] initWithCGImage: image.CGImage scale: 1.0 orientation: UIImageOrientationLeftMirrored];
    
    
    NSDictionary* imageOptions = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:6] forKey:CIDetectorImageOrientation];
    CIContext *context = [CIContext contextWithOptions:nil];
    NSDictionary *options = @{CIDetectorAccuracy : CIDetectorAccuracyLow};
    CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeFace context:context options:options];
    
    CIImage *ciImage = [CIImage imageWithCGImage:image.CGImage];
    
    NSArray *features = [detector featuresInImage:ciImage options:imageOptions];
    
    for (CIFaceFeature *f in features) {
        CGRect faceBounds = [f bounds];
        
        CGFloat test = faceBounds.size.height / image.size.height * 100;
        if (badRangeCounter == 5) {
            //tell user, reset board
            dispatch_async(dispatch_get_main_queue(), ^{
                //[self textToSpeech:badRangeText];
                badRangeCounter = 0;
                badDistanceLabel.text = badRangeText;
                startButton.backgroundColor = [UIColor redColor];
                if (self.allowedToReset) [self prepareLogmarSetup:currentLogmarValue];
            });
        }
        if (test >= 35 && test <= 45) {
            inGoodRange = true;
            badRangeCounter = 0;
            dispatch_async(dispatch_get_main_queue(), ^{
                badDistanceLabel.text = @"Good Distance";//[NSString stringWithFormat:@"Good Distance, %f", test];
                startButton.backgroundColor = [UIColor blueColor];
            });
        } else if (test < 35) {
            inGoodRange = false;
            badRangeCounter++;
            badRangeText = @"You are too far away. Please move closer. Resetting board...";
        } else if (test > 45) {
            inGoodRange = false;
            badRangeText = @"You are too close. Please move farther away. Resetting board...";
            badRangeCounter++;
        }
    }
    if (features.count == 0) {
        noPersonInFrame++;
    } else {
        noPersonInFrame = 0;
    }
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        
        //NSLog(@"No Person: %d", noPersonInFrame);
        //NSLog(@"Bad Range Ctr: %d", badRangeCounter);
        //NSLog(@"Bad Range Text: %@", badRangeText);
        //NSLog((inGoodRange) ? @"" : @"Bad Range");
    });
    
}

// Create a UIImage from sample buffer data
- (UIImage *) imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer {
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    // Lock the base address of the pixel buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    // Get the number of bytes per row for the pixel buffer
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
    
    // Get the number of bytes per row for the pixel buffer
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    // Get the pixel buffer width and height
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    // Create a device-dependent RGB color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // Create a bitmap graphics context with the sample buffer data
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8,
                                                 bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    // Create a Quartz image from the pixel data in the bitmap graphics context
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    // Unlock the pixel buffer
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    // Free up the context and color space
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    // Create an image object from the Quartz image
    UIImage *image = [UIImage imageWithCGImage:quartzImage];
    
    // Release the Quartz image
    CGImageRelease(quartzImage);
    
    return (image);
}

#pragma mark - Speech to Text/Text to Speech

/* Synthesizes the string to speech
 */
- (void)watsonSaysHi:(NSNotification *)notification {
    [self textToSpeech:(NSString *)notification.object];
}

/* Converts from text to speech
 */
- (void)textToSpeech:(NSString*) msg {
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"watson" object:msg];
    
    NSString *str = msg;//(NSString *)notification.object;
    speech.text = str;
    if (synth == nil) {
        synth = [[AVSpeechSynthesizer alloc] init];
        synth.delegate = self;
    }
    AVSpeechUtterance *utterance = [AVSpeechUtterance speechUtteranceWithString:str];
    utterance.rate = 0.45;
    self.utteringThings = true;
    [synth speakUtterance:utterance];
    while(self.utteringThings) {
        [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:.25]];
        NSLog(@"Speaking...");
    }
}

/* Listens for user speech
 */
- (void)listenForSpeech {
    self.speechDone = false;
    STTConfiguration *conf = [[STTConfiguration alloc] init];
    [conf setBasicAuthUsername:@"aba6d11b-e500-482b-8930-8deee0381a54"];
    [conf setBasicAuthPassword:@"pVQH0ixv6Sjs"];
    SpeechToText* stt = [SpeechToText initWithConfig:conf];
    NSLog(@"Listening");
    [stt recognize:^(NSDictionary* res, NSError* err){
        if(err == nil) {
            if([stt isFinalTranscript:res]) {
                [stt endRecognize];
                self.speechDone = true;
            }
            self.speechText = [stt getTranscript:res];
        }
    }];
    [stt getPowerLevel:^(float power){
        NSLog(@"Power = %a", power);
        
    }];
    while(!self.speechDone) [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:.25]];
}

/* Determines if words said by user are the four direction
 * words accepted by the program and filters out all invalid words.
 */
- (NSArray*) toDirections:(NSArray*) arr
{
    NSMutableArray* arrOutputs = [NSMutableArray new];
    for(NSString* str in arr)
    {
        if([str isEqual: @"left"] || [str isEqual: @"right"]
           || [str isEqual: @"down"] || [str isEqual: @"up"])
        {
            [arrOutputs addObject:str];
            if(arrOutputs.count > 5) [arrOutputs removeObjectAtIndex:0];
        }
    }
    if(arrOutputs.count < 5) return nil;
    return arrOutputs;
}

/* Listens to the user
 */
- (void)listenAgain:(id)sender {
    self.allowedToReset = false;
    NSArray* arr;
    [self listenForSpeech];
    self.speechText = [self.speechText stringByReplacingOccurrencesOfString:@"that" withString:@"left"];
    self.speechText = [self.speechText stringByReplacingOccurrencesOfString:@"okay" withString:@"up"];
    self.speechText = [self.speechText stringByReplacingOccurrencesOfString:@"ok" withString:@"up"];
    self.speechText = [self.speechText stringByReplacingOccurrencesOfString:@"dial" withString:@"down"];
    arr = [self toDirections:[self.speechText componentsSeparatedByString:@" "]];
    if(arr) {
        [self textToSpeech: [NSString stringWithFormat:@"Did you say %@?", arr]];
        
        [self listenForSpeech];
        if([self.speechText containsString:@"yes"] || [self.speechText containsString:@"yeah"] || [self.speechText containsString:@"of course"])
        {
            spokenOrientations = arr;
            NSLog(@"%@", arr);
            int numCorrect = [self compare];
            if (numCorrect < 4) {
                int eyesight = .5 + 20 * pow(10, currentLogmarValue / 10.0);
                if(eyesight > 100) eyesight = (eyesight + 5)/10 * 10;
                [self textToSpeech:[NSString stringWithFormat:@"Your Eyesight Is: 20, %d", eyesight]];
                startButton.enabled = false;
                startButton.userInteractionEnabled = false;
                startButton.hidden = true;
            } else {
                currentLogmarValue -= 0.1;
                [startButton setTitle:@"Continue" forState:UIControlStateNormal];
            }
            self.allowedToReset = true;
            return;
        }
    }
    [self textToSpeech: @"I didn't catch that. Try again"];
    self.allowedToReset = true;
}

/* Compare show many of the five images displayed the user got correct
 */
- (int)compare {
    int counter = 0;
    for (int i = 0; i < 5; i++) {
        if ([currentOrientations[i] isEqual: spokenOrientations[i]]) {
            counter++;
            NSLog(@"equal: %d", i);
        }
    }
    return counter;
}

#pragma mark - Generation of Images

/* Generate "E"s in random orientations
 */
- (NSArray *)generateRandomOrientations {
    NSMutableArray *arr = [NSMutableArray new];
    
    for (int i = 0; i < 5; i++) {
        int randomLoc = arc4random() % 4;
        [arr addObject:orientations[randomLoc]];
    }
    
    return arr;
}

/* Draws out the generation of the E's
 */
- (void)generateFormation:(CGFloat)logmarVal {
    CGFloat inchPixelFactor = 96;
    
    currentOrientations = [self generateRandomOrientations];
    NSLog(@"%@", currentOrientations);
    
    CGFloat logmar = M_PI / 144.0 * pow(10, logmarVal);
    CGFloat height = logmar * inchPixelFactor;
    NSLog(@"%f", height);
    
    CGFloat usedSpace = height * 5 + 20 * 4;
    
    float currentSpace = (self.view.frame.size.width - usedSpace)/2.0;
    float xLoc = currentSpace;
    
    for (NSString *str in currentOrientations) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(xLoc, 88, height, height)];
        //imageView.frame = CGRectMake(xLoc, 88, 2.5, 2.5);
        imageView.image = [UIImage imageNamed:[str stringByAppendingString:(logmarVal > 0.5) ? @".PNG" :  @"_xsmall.PNG"]];
        [self.view addSubview:imageView];
        xLoc += (height + 20);
        [images addObject:imageView];
    }
}

/* Determines that the stepper has changed and
 * the user changed what they are looking at.
 */
- (void)stepperChanged:(id)sender {
    int value = ((UIStepper *)sender).value;
    currentLogmarValue = value;
    [self prepareLogmarSetup:value];
}

/* Prepares Logmar image setup
 */
- (void)prepareLogmarSetup:(int)logmarVal {
    for (UIImageView *imgView in images) {
        [imgView removeFromSuperview];
    }
    [images removeAllObjects];
    [self generateFormation:logmarVal / 10.0];
    label.text = [NSString stringWithFormat:@"%f", logmarVal / 10.0];
}

- (void)startButton:(id)sender {
    if ([startButton.titleLabel.text isEqualToString:@"Start"]) {
        [startButton setTitle:@"Tap to speak" forState:UIControlStateNormal];
        currentLogmarValue = 13;

        [self prepareLogmarSetup:currentLogmarValue];
        [self setupMotion];
    } else if ([startButton.titleLabel.text isEqualToString:@"Tap to speak"]) {
        [self listenAgain:nil];
    } else {
        [startButton setTitle:@"Tap to speak" forState:UIControlStateNormal];
        [self prepareLogmarSetup:currentLogmarValue];
    }
}

#pragma mark - AVSpeechSynthesizer

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didStartSpeechUtterance:(nonnull AVSpeechUtterance *)utterance {
    NSLog(@"Begin speaking");
}


- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance {
    self.utteringThings = false;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  VisionTestViewController.h
//  Instant Acuity
//
//  Created by Perry Alagappan on 2/14/16.
//  Copyright © 2016 TreeHacks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <watsonsdk/SpeechToText.h>
#import <watsonsdk/STTConfiguration.h>
#import <watsonsdk/TextToSpeech.h>
#import <watsonsdk/TTSConfiguration.h>

#import <CoreMotion/CoreMotion.h>
#import <AVFoundation/AVFoundation.h>

@interface VisionTestViewController : UIViewController <AVCaptureVideoDataOutputSampleBufferDelegate, AVSpeechSynthesizerDelegate> {
    NSArray *orientations;
    NSMutableArray *images;
    
    IBOutlet UILabel *label;
    
    IBOutlet UILabel *speech;
    
    NSArray *spokenOrientations;
    NSArray *currentOrientations;
    
    int currentLogmarValue;
    
    AVCaptureSession *session;
    AVCaptureDevice *device;
    AVCaptureDeviceInput *input;
    AVCaptureVideoDataOutput *output;
    AVCaptureConnection *connection;
    
    CMMotionManager *motionManager;
    CMDeviceMotion *deviceMotion;
    
    CGFloat pitch, yaw, roll;
    
    int noPersonInFrame, badRangeCounter;
    BOOL inGoodRange;
    NSString *badRangeText;
    
    IBOutlet UIButton *startButton, *finishButton;

    AVSpeechSynthesizer *synth;
    
    IBOutlet UILabel *badDistanceLabel;
}

- (void)generateFormation:(CGFloat)logmarVal;
- (NSArray *)generateRandomOrientations;

- (IBAction)stepperChanged:(id)sender;
- (IBAction)listenAgain:(id)sender;

- (IBAction)startButton:(id)sender;

@property (nonatomic, retain) CIDetector *faceDetector;

@end
